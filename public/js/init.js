(function($){
  $(function(){

    $('.sidenav').sidenav();
    $('.parallax').parallax();
    $('.datepicker').datepicker();
    $('.fixed-action-btn').floatingActionButton();
    $('select').formSelect();
    $('.modal').modal();

  }); // end of document ready
})(jQuery); // end of jQuery name space
