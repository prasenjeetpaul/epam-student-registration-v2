// import React from 'react';
// import ReactDOM from 'react-dom';
// import './index.css';
// import App from './../src/js/containers/App';

// ReactDOM.render(<App />, document.getElementById('root'));


import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";

import './index.css';

import store from "./js/store/store";
import App from "./js/containers/App";
import * as serviceWorker from './serviceWorker';

window.React = React;

render(
    <BrowserRouter>
        <Provider store={store}>
            <App />
        </Provider>
    </BrowserRouter>,
    document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
