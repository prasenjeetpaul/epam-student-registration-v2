import { APP_STATE_ACTION } from "./../actions/actionType";
import appState from "./../store/appState";

export default function(state = appState, action) {
    switch(action.type) {
        case APP_STATE_ACTION.PATH_CHANGED: {
            return {
                ...state,
                viewPath: action.payload
            }
        }
        case APP_STATE_ACTION.LOGIN_SUCCESS: {
            return {
                ...state,
                isLoggedIn: true
            }
        }

        case APP_STATE_ACTION.LOGIN_FAILED: {
            return {
                ...state,
                loginError: action.payload
            }
        }

        case APP_STATE_ACTION.REGISTER_FAILED: {
            return {
                ...state,
                registerError: action.payload
            }
        }

        case APP_STATE_ACTION.LOGOUT_CLICKED: {
            return {
                ...state,
                isLoggedIn: false,
                studentID: null,
            }
        }

        case APP_STATE_ACTION.REGISTER_SUCCESS: {
            return {
                ...state,
                isLoggedIn: true,
                studentID: action.payload
            }
        }

        case APP_STATE_ACTION.PEROSNAL_DATA_FILLED: {
            return {
                ...state,
                isPersonalFilled: action.payload
            }
        }

        case APP_STATE_ACTION.ADDRESS_DATA_FILLED: {
            return {
                ...state,
                isAddressFilled: action.payload
            }
        }

        case APP_STATE_ACTION.ACADEMIC_DATA_FILLED: {
            return {
                ...state,
                isAcademicFilled: action.payload
            }
        }

        case APP_STATE_ACTION.DOCUMENT_UPLOADED: {
            return {
                ...state,
                isDocumentUploaded: action.payload
            }
        }

        case APP_STATE_ACTION.SAVE_API_LINK: {
            return {
                ...state,
                API: action.payload
            }
        }

        default: {
            return state;
        }
    }
}