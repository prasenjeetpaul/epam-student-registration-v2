import { ADDRESS_DETAIL_ACTION } from "../actions/actionType";
import addressDetailState from "../store/addressDetailState";

export default function(state = addressDetailState, action) {
    switch(action.type) {
        case ADDRESS_DETAIL_ACTION.TYPE_ENTERED: {
            return {
                ...state,
                type: action.payload
            }
        }

        case ADDRESS_DETAIL_ACTION.HOUSE_NUMBER_ENTERED: {
            return {
                ...state,
                houseNumber: action.payload
            }
        }

        case ADDRESS_DETAIL_ACTION.STREET_ENTERED: {
            return {
                ...state,
                street: action.payload
            }
        }

        case ADDRESS_DETAIL_ACTION.LANDMARK_ENTERED: {
            return {
                ...state,
                landmark: action.payload
            }
        }

        case ADDRESS_DETAIL_ACTION.CITY_ENTERED: {
            return {
                ...state,
                city: action.payload
            }
        }

        case ADDRESS_DETAIL_ACTION.STATE_ENTERED: {
            return {
                ...state,
                state: action.payload
            }
        }

        case ADDRESS_DETAIL_ACTION.ZIPCODE_ENTERED: {
            return {
                ...state,
                zipcode: action.payload
            }
        }

        case ADDRESS_DETAIL_ACTION.COUNTRY_ENTERED: {
            return {
                ...state,
                country: action.payload
            }
        }

        case ADDRESS_DETAIL_ACTION.SELECTED_COUNTRY_ENTERED: {
            return {
                ...state,
                selectedCountry: action.payload,
                selectedState: "",
                state: null
            }
        }

        case ADDRESS_DETAIL_ACTION.SELECTED_STATE_ENTERED: {
            return {
                ...state,
                selectedState: action.payload
            }
        }

        case ADDRESS_DETAIL_ACTION.ADDRESS_DATA_LOAD: {
            return {
                ...action.payload
            }
        }

        default: {
            return state;
        }
    }
}