import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { Academic_Action } from "../actions/academicDetailAction";
import { changePath } from "../actions/appStateAction";
import InputTag from "../components/InputTag";
import Spinner from "../components/Spinner";
import AcademicDetailCard from "./AcademicDetailCard";
import M from "materialize-css";

class AcademicDetail extends React.Component {
    constructor(props) {
        super(props);
        this.nextClicked = this.nextClicked.bind(this);
        this.state = {
            spinnerClass: ["hide"],
            detailCount: 1,
            maxDetailCount: 5,
        }
        this.addCard = this.addCard.bind(this);
        this.detailCard = [];
        this.deleteCard = this.deleteCard.bind(this);
    }
    
    addCard() {
        this.setState({
            detailCount: this.state.detailCount + 1,
        })
    }

    deleteCard(id) {
        this.props.deleteRecord(id-1);
        this.detailCard.splice( id, 1 );
        this.setState({
            detailCount: this.state.detailCount - 1,
        })
    }

    nextClicked(event) {
        event.preventDefault();
        let highEduArray = this.props.isHighestEducation;
        if (!highEduArray.includes("true")) {
            M.toast({html: 'Atleast one highest eductaion is required', classes: 'rounded'});
            return;
        } else {
            var highCount = 0;
            for(var i = 0; i < highEduArray.length; i++){
                if(highEduArray[i] == "true")
                highCount++;
            }
            if (highCount > 1) {
                M.toast({html: 'Only one highest eductaion is allowed', classes: 'rounded'});
                return;
            }
        }
        this.props.academicDetailSubmitted(this.props.history);
        this.setState({
            spinnerClass: [""]
        })
    }

    

    render() {
        this.detailCard = [];
        for (let i = 1 ; i <= this.state.detailCount; i++) {
            if (i > this.state.maxDetailCount) {
                M.toast({html: 'Cannot add more than 5 academic records', classes: 'rounded'});
                break;
            }
            if (i==1 || i < this.state.detailCount) {
                this.detailCard.push(<AcademicDetailCard 
                    key={i} 
                    id={i} 
                    showDelete={false}/>);
            } else {
                this.detailCard.push(<AcademicDetailCard 
                    key={i} 
                    id={i} 
                    showDelete={true}
                    deleteAction={() => this.deleteCard(i)}/>);
            }
            
        }
        

        return(
            <div className="container">
                <div className="section card z-depth-3 auth-card">
        
                    <div className="row center">
                        <div className="col s12 m8 offset-m2">
                            <h4 className="header light">Academic Details</h4>
                            <h6 className="header light">Enter your academic details</h6>
                        </div>
                    </div>
                    <form onSubmit={this.nextClicked}>
                    
                    {/* <div className="row">
                        <div className="input-field  col s12 m8 offset-m2">
                            <select class="browser-default">
                            <option value="" disabled selected>Address Type</option>
                            <option value="Permanent">Permanent</option>
                            <option value="Temporary">Temporary</option>
                            </select>
                        </div>
                    </div> */}

                    {
                        this.detailCard
                    }

                    <div className="row center">
                        <Spinner className={this.state.spinnerClass.join(" ")} />
                    </div>
                    
                    <div className="row center">
                        <div className="input-field col s12 m4 left">
                            <button 
                                className="btn waves-effect waves-light" 
                                name="action"
                                onClick={() => this.props.changePath("/edit/address")}>Prev
                            <i className="material-icons left">chevron_left</i>
                            </button>
                        </div>
                        <div className="input-field col s12 m4 right">
                            <button className="btn waves-effect waves-light" type="submit" name="action">Next
                            <i className="material-icons right">chevron_right</i>
                            </button>
                        </div>
                    </div>
                    </form>
                </div>
                
                <div className="section">
                    <div className="fixed-action-btn">
                        <a className="btn-floating btn-large green" onClick={this.addCard}>
                            <i className="large material-icons">add</i>
                        </a>
                    </div>
                </div>




                <div className="section">
                    <div className="fixed-action-btn">
                        <a className="btn-floating btn-large green" onClick={this.addCard}>
                            <i className="large material-icons">add</i>
                        </a>
                    </div>
                </div>

                
            </div>
        )














        //return (
        //     <section class="main-auth-content">
        //         <div class="auth-content">
        //             <form>
        //                 <span class="auth-text">Enter your Academic Details</span>
        //                 <InputTag
        //                     type="text"
        //                     placeholder="Institute Name"
        //                     value={this.props.name}
        //                     onChange={(event) => this.props.nameEntered(event.target.value)}/>
        //                 <InputTag
        //                     type="text"
        //                     placeholder="Board Name"
        //                     value={this.props.board}
        //                     onChange={(event) => this.props.boardEntered(event.target.value)}/>
        //                 <InputTag
        //                     type="number"
        //                     placeholder="Passout Year"
        //                     value={this.props.passoutYear}
        //                     onChange={(event) => this.props.passOutYearEntered(event.target.value)}/>
        //                 <InputTag
        //                     type="number"
        //                     placeholder="Percentage"
        //                     value={this.props.percentageOfMarks}
        //                     onChange={(event) => this.props.percentageOfMarksEntered(event.target.value)}/>
        //                 <InputTag
        //                     type="text"
        //                     placeholder="Is Highest Education?"
        //                     value={this.props.isHighestEducation}
        //                     onChange={(event) => this.props.isHighestEducationEntered(event.target.value)}/>
        //             </form>
        //             <div>
        //                 <Link to="/edit/address"><button class="btn next-btn">PREV</button></Link>
        //                 <button class="btn next-btn" type="submit"  onClick={this.nextClicked}>NEXT</button>
        //             </div>

        //             <Spinner className={this.state.spinnerClass.join(" ")} />
        //         </div>
        //     </section>
        // )
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        nameEntered: Academic_Action.nameEntered,
        boardEntered: Academic_Action.boardEntered,
        passOutYearEntered: Academic_Action.passOutYearEntered,
        percentageOfMarksEntered: Academic_Action.percentageOfMarksEntered,
        isHighestEducationEntered: Academic_Action.isHighestEducationEntered,
        academicDetailSubmitted: Academic_Action.academicDetailSubmitted,
        
        changePath: changePath,
        deleteRecord: Academic_Action.deleteRecord,
    }, dispatch)
);

const mapStateToProps = state => ({
    name: state.academicDetail.name,
    board: state.academicDetail.board,
    passOutYear: state.academicDetail.passOutYear,
    percentageOfMarks: state.academicDetail.percentageOfMarks,
    isHighestEducation: state.academicDetail.isHighestEducation
});

export default connect(mapStateToProps, mapDispatchToProps)(AcademicDetail);