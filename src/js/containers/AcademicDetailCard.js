import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Academic_Action } from "../actions/academicDetailAction";
import { changePath } from "../actions/appStateAction";

class AcademicDetailCard extends React.Component {
    constructor(props) {
        super(props);
        this.styles = {
            margin: "20px",
            border: "1px solid #579BFB",
            paddingTop: "10px", 
          };
    }
    render() {
        let { id } = this.props;
        return (
            <div style={this.styles}>
                
                {
                    !this.props.showDelete
                    ?   null
                    :   <div className="row right-align">
                            <div className="input-field col s12">
                                <a  className="btn-floating btn-large waves-effect waves-light red"
                                    onClick={this.props.deleteAction}>
                                    <i className="material-icons">delete</i>
                                </a>
                            </div>
                        </div>
                }
                

                <div className="row">
                    <div className="input-field col s12 m8 offset-m2">
                        <i className="material-icons prefix">email</i>
                        <input 
                            id={"name" + id} 
                            type="text" 
                            className="validate" 
                            value={this.props.name[id]}
                            onChange={(event) => this.props.nameEntered(event.target.value, id)}
                            required />
                        <label htmlFor={"name" + id}>Institute Name</label>
                    </div>
                </div>

                <div className="row">
                    <div className="input-field col s12 m8 offset-m2">
                        <i className="material-icons prefix">email</i>
                        <input 
                            id={"board" + id}
                            type="text" 
                            className="validate" 
                            value={this.props.board[id]}
                            onChange={(event) => this.props.boardEntered(event.target.value, id)} 
                            required/>
                        <label htmlFor={"board" + id}>Borad/University</label>
                    </div>
                </div>
                
                <div className="row">
                    <div className="input-field col s12 m8 offset-m2">
                        <i className="material-icons prefix">email</i>
                        <input 
                            id={"passOutYear" + id}
                            type="number" 
                            className="validate" 
                            value={this.props.passOutYear[id]}
                            onChange={(event) => this.props.passOutYearEntered(event.target.value, id)} 
                            required/>
                        <label htmlFor={"passOutYear" + id}>Passout Year</label>
                    </div>
                </div>

                <div className="row">
                    <div className="input-field col s12 m8 offset-m2">
                        <i className="material-icons prefix">email</i>
                        <input 
                            id={"percentageOfMarks"  + id}
                            type="number" 
                            className="validate" 
                            value={this.props.percentageOfMarks[id]}
                            onChange={(event) => this.props.percentageOfMarksEntered(event.target.value, id)}
                            required />
                        <label htmlFor={"percentageOfMarks"  + id}>Percentage</label>
                    </div>
                </div>
                {/* {
                    this.props.isHighestEducation.includes("true") && this.props.isHighestEducation[id] != "true"
                    ?   null
                    :   
                } */}

                <div className="row">
                    <div className="input-field col s12 m8 offset-m2">
                        <p>
                        <label>
                            <span>Is Highest Education:</span>
                        </label>
                        </p>
                        <p>
                        <label>
                            <input 
                                name={"group" + id} 
                                type="radio" 
                                value="true"
                                defaultChecked={this.props.isHighestEducation[id] == "true"}
                                onChange={event => this.props.isHighestEducationEntered(event.currentTarget.value, id)} 
                                required />
                            <span>Yes</span>
                        </label>
                        </p>
                        <p>
                        <label>
                            <input 
                                name={"group" + id} 
                                type="radio" 
                                value="false"
                                defaultChecked={this.props.isHighestEducation[id] == "false"}
                                onChange={event => this.props.isHighestEducationEntered(event.currentTarget.value, id)}
                                required/>
                            <span>No</span>
                        </label>
                        </p>
                    </div>
                </div>
                

            </div>
        )
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        nameEntered: Academic_Action.nameEntered,
        boardEntered: Academic_Action.boardEntered,
        passOutYearEntered: Academic_Action.passOutYearEntered,
        percentageOfMarksEntered: Academic_Action.percentageOfMarksEntered,
        isHighestEducationEntered: Academic_Action.isHighestEducationEntered,
        academicDetailSubmitted: Academic_Action.academicDetailSubmitted,
        
        changePath: changePath
    }, dispatch)
);

const mapStateToProps = state => ({
    name: state.academicDetail.name,
    board: state.academicDetail.board,
    passOutYear: state.academicDetail.passOutYear,
    percentageOfMarks: state.academicDetail.percentageOfMarks,
    isHighestEducation: state.academicDetail.isHighestEducation
});

export default connect(mapStateToProps, mapDispatchToProps)(AcademicDetailCard);