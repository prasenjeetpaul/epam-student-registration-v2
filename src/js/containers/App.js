import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
// import { Switch, Route} from "react-router-dom";
import Home from "./Home";
import AuthScreen from "./AuthScreen";
// import "../../css/materialize.css";
import "../../css/materialize.min.css";
import "../../css/style.css";
import "../../css/admin.css";
import { saveUserID, saveAPI } from "./../actions/registerAction";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = null;
    }

    static getDerivedStateFromProps(props, state) {
        let userId = document.getElementById("userId");
        props.saveUserID(userId);

        let API = document.getElementById("contextPath");
        props.saveAPI(API);
        return state;
    }

    render() {
        

        if (this.props.studentID == null) {
            return <AuthScreen />
        }else if (this.props.viewPath === "/" || this.props.viewPath.substring(0, 5) === "/edit") {
            return <Home />
        } else {
            return <AuthScreen />
        }
        // return (
        //     <Switch>
        //         <Route exact path="/" component={Home} />
        //         <Route path="/edit" component={Home} />
        //         <Route path="*" component={AuthScreen} />
        //     </Switch>
        // )
    }
}

const mapStateToProps = state => ({
    viewPath: state.appState.viewPath,
    studentID: state.appState.studentID,
})

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        saveUserID: saveUserID,
        saveAPI: saveAPI
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(App);