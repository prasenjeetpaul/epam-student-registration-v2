import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Redirect, Link } from "react-router-dom";
import { registerEmailEntered, registerPassword1Entered, registerPassword2Entered, registerClicked} from "../actions/registerAction";
import { changePath } from "./../actions/appStateAction";
import InputTag from "../components/InputTag";
import Button from "../components/Button";
import ErrorMessage from "../components/ErrorMessage";
import { LoginAPI } from "./../store/appState";

class Register extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.registerClicked();
    }

    render() {
        return(
            <div className="container">
            <div className="section card z-depth-3 auth-card">
            <form onSubmit={this.handleSubmit}>

                <div className="row center">
                    <div className="col s12 m8 offset-m2">
                        <h4 className="header light">REGISTER</h4>
                    </div>
                </div>

                <div className="row">
                    <div className="input-field col s12 m8 offset-m2">
                        <i className="material-icons prefix">email</i>
                        <input 
                            id="emailId" 
                            type="email" 
                            className="validate" 
                            value={this.props.emailID}
                            onChange={event => this.props.registerEmailEntered(event.target.value)}
                            required/>
                        <label htmlFor="emailId">Email ID</label>
                    </div>
                </div>

                <div className="row">
                    <div className="input-field col s12 m8 offset-m2">
                        <i className="material-icons prefix">lock</i>
                        <input 
                            id="password1" 
                            type="password" 
                            className="validate" 
                            value={this.props.password1}
                            onChange={event => this.props.registerPassword1Entered(event.target.value)}
                            required/>
                        <label htmlFor="password1">Password</label>
                    </div>
                </div>

                <div className="row">
                    <div className="input-field col s12 m8 offset-m2">
                        <i className="material-icons prefix">lock</i>
                        <input 
                            id="password2" 
                            type="password" 
                            className="validate" 
                            value={this.props.password2}
                            onChange={event => this.props.registerPassword2Entered(event.target.value)}
                            required/>
                        <label htmlFor="password2">Confirm Password</label>
                    </div>
                </div>

                {
                    this.props.registerError != null 
                    ? <ErrorMessage message={this.props.registerError}/>
                    : null
                }
                
                <div className="row center">
                    <div className="input-field col s12 m8 offset-m2">
                        <button className="btn waves-effect waves-light" type="submit" name="action">REGISTER
                        <i className="material-icons right">send</i>
                        </button>
                    </div>
                </div>

                <div className="row center">
                    <div className="input-field col s12 m8 offset-m2">
                        <span>
                            Already registered? 
                            {/* <a onClick={() => this.props.changePath("/login")}> Login now</a> */}
                            <a href={LoginAPI}> Login now</a>
                        </span>
                    </div>
                </div>
                
            </form>
            </div>
            </div>
        )





        /*
        if (this.props.isLoggedIn) {
            return <Redirect to="/"/>
        }
        return (
            <div className="auth-content">
                <form onSubmit={this.handleSubmit}>
                    <span className="auth-text">REGISTER</span>
                    <InputTag
                        type="text"
                        placeholder="Email"
                        value={this.props.emailID}
                        onChange={event => this.props.registerEmailEntered(event.target.value)} />
                    <InputTag
                        type="password"
                        placeholder="Password"
                        value={this.props.password1}
                        onChange={event => this.props.registerPassword1Entered(event.target.value)} />
                    <InputTag
                        type="password"
                        placeholder="Confirm Password"
                        value={this.props.password2}
                        onChange={event => this.props.registerPassword2Entered(event.target.value)} />
                    <Button
                        type="submit"
                        label="REGISTER" />
                </form>
                <div className="auth-helper">
                    {
                        this.props.registerError != null 
                        ? <ErrorMessage message={this.props.registerError}/>
                        : null
                    }
                    <span>
                        Already registered? <Link to="/login">Login now</Link>
                    </span>
                </div>
            </div>
        )*/
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        registerEmailEntered: registerEmailEntered,
        registerPassword1Entered: registerPassword1Entered,
        registerPassword2Entered: registerPassword2Entered,
        registerClicked: registerClicked,
        changePath: changePath
    }, dispatch)
);

const mapStateToProps = state => ({
    emailID: state.register.emailId,
    password1: state.register.password1,
    password2: state.register.password2,
    isLoggedIn: state.appState.isLoggedIn,
    registerError: state.appState.registerError
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);