import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { firstNameEntered, 
            middleNameEntered, 
            lastNameEntered, 
            dateOfBirthEntered, 
            mobileNumberEntered, 
            personalDetailSubmitted,
            fillPersonalDetails } from "../actions/personalDetailAction";
import { changePath } from "../actions/appStateAction";
import M from "materialize-css";

import InputTag from "../components/InputTag";
import Spinner from "../components/Spinner";

class PersonalDetail extends React.Component {
    constructor(props) {
        super(props);
        this.nextClicked = this.nextClicked.bind(this);
        this.state = {
            spinnerClass: ["hide"]
        }
        this.props.fillPersonalDetails();
    }

    nextClicked(event) {
        event.preventDefault();
        this.props.personalDetailSubmitted(this.props.history);
        this.setState({
            spinnerClass: [""]
        })
    }

    componentDidUpdate() {
        M.updateTextFields();
    }

    render() {
        return (
            <div className="container">
                <div className="section card z-depth-3 auth-card">
        
                    <div className="row center">
                        <div className="col s12 m8 offset-m2">
                            <h4 className="header light">Peronal Details</h4>
                            <h6 className="header light">Enter your personal details</h6>
                        </div>
                    </div>
                    <form onSubmit={this.nextClicked}>
                    <div className="row">
                        <div className="input-field col s12 m8 offset-m2">
                            <i className="material-icons prefix">person</i>
                            <input 
                                id="firstName" 
                                type="text" 
                                className="validate" 
                                value={this.props.firstName}
                                onChange={(event) => this.props.firstNameEntered(event.target.value)}
                                required />
                            <label htmlFor="firstName">First Name</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s12 m8 offset-m2">
                            <i className="material-icons prefix">person</i>
                            <input 
                                id="middleName" 
                                type="text" 
                                className="validate" 
                                value={this.props.middleName}
                                onChange={(event) => this.props.middleNameEntered(event.target.value)}/>
                            <label htmlFor="middleName">Middle Name</label>
                        </div>
                    </div>
                    
                    <div className="row">
                        <div className="input-field col s12 m8 offset-m2">
                            <i className="material-icons prefix">person</i>
                            <input 
                                id="lastName" 
                                type="text" 
                                className="validate" 
                                value={this.props.lastName}
                                onChange={(event) => this.props.lastNameEntered(event.target.value)}
                                required />
                            <label htmlFor="lastName">Last Name</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s12 m8 offset-m2">
                            <i className="material-icons prefix">date_range</i>
                            <input 
                                id="dateOfBirth" 
                                type="date" 
                                className="datepicker" 
                                value={this.props.dateOfBirth}
                                onChange={(event) => this.props.dateOfBirthEntered(event.target.value)}
                                required />
                            <label htmlFor="dateOfBirth">Date of Birth</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s12 m8 offset-m2">
                            <i className="material-icons prefix">call</i>
                            <input 
                                id="mobileNumber" 
                                type="number" 
                                className="validate" 
                                value={this.props.mobileNumber}
                                onChange={(event) => this.props.mobileNumberEntered(event.target.value)}
                                required />
                            <label htmlFor="mobileNumber">Phone Number</label>
                        </div>
                    </div>
                    
                    <div className="row center">
                        <Spinner className={this.state.spinnerClass.join(" ")} />
                    </div>
                    
                    <div className="row center">
                        <div className="input-field col s12 m4 left">
                            {/* <button 
                                className="btn waves-effect waves-light" 
                                onClick={() => this.props.changePath("/edit/personal")}>Prev
                            <i className="material-icons left">chevron_left</i>
                            </button> */}
                        </div>
                        <div className="input-field col s12 m4 right">
                            <button className="btn waves-effect waves-light" type="submit" name="action">Next
                            <i className="material-icons right">chevron_right</i>
                            </button>
                        </div>
                    </div>
                    </form>
                </div>

            </div>
        )











        // return (
        //     <section className="main-auth-content">
        //         <div className="auth-content">
        //             <form onSubmit={this.nextClicked}>
        //                 <span className="auth-text">Enter your Personal Details</span>
        //                 <InputTag
        //                     type="text"
        //                     placeholder="First Name"
        //                     value={this.props.firstName}
        //                     onChange={(event) => this.props.firstNameEntered(event.target.value)}/>
        //                 <InputTag
        //                     type="text"
        //                     placeholder="Middle Name"
        //                     value={this.props.middleName}
        //                     onChange={(event) => this.props.middleNameEntered(event.target.value)}/>
        //                 <InputTag
        //                     type="text"
        //                     placeholder="Last Name"
        //                     value={this.props.lastName}
        //                     onChange={(event) => this.props.lastNameEntered(event.target.value)}/>
        //                 <InputTag
        //                     type="date"
        //                     placeholder="Date of Birth (DD-MM-YYYY)"
        //                     value={this.props.dateOfBirth}
        //                     onChange={(event) => this.props.dateOfBirthEntered(event.target.value)}/>
        //                 <InputTag
        //                     type="number"
        //                     placeholder="Mobile Number"
        //                     value={this.props.mobileNumber}
        //                     onChange={(event) => this.props.mobileNumberEntered(event.target.value)}/>
        //             </form>
        //             <div>
        //                 {/* <button class="btn next-btn" type="submit"> PREV</button> */}
        //                 <button class="btn next-btn" type="submit" onClick={this.nextClicked}>NEXT</button>
        //             </div>

        //             <Spinner className={this.state.spinnerClass.join(" ")} />
        //         </div>
        //     </section>
        // )
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        firstNameEntered: firstNameEntered,
        middleNameEntered: middleNameEntered,
        lastNameEntered: lastNameEntered,
        dateOfBirthEntered: dateOfBirthEntered,
        mobileNumberEntered: mobileNumberEntered,
        personalDetailSubmitted: personalDetailSubmitted,

        changePath: changePath,
        fillPersonalDetails: fillPersonalDetails,
    }, dispatch)
);

const mapStateToProps = state => ({
    firstName: state.personalDetail.firstName,
    middleName: state.personalDetail.middleName,
    lastName: state.personalDetail.lastName,
    dateOfBirth: state.personalDetail.dateOfBirth,
    mobileNumber: state.personalDetail.mobileNumber
});

export default connect(mapStateToProps, mapDispatchToProps)(PersonalDetail);