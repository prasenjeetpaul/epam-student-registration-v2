import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { AddressAction } from "../actions/addressDetailAction";
import { changePath } from "../actions/appStateAction";
import InputTag from "../components/InputTag";
import Spinner from "../components/Spinner";
import Dropdown from "./Dropdown";
import M from "materialize-css";

class AddressDetail extends React.Component {
    constructor(props) {
        super(props);
        this.nextClicked = this.nextClicked.bind(this);
        this.state = {
            spinnerClass: ["hide"],
        }
        // this.props.fetchCountry();
        this.props.fillAddressDetails();
    }

    nextClicked(event) {
        event.preventDefault();
        this.props.addressDetailSubmitted(this.props.history);
        this.setState({
            spinnerClass: [""]
        })
    }

    componentDidUpdate() {
        M.updateTextFields();
    }

    render() {

        return (
            <div className="container">
                <div className="section card z-depth-3 auth-card">
        
                    <div className="row center">
                        <div className="col s12 m8 offset-m2">
                            <h4 className="header light">Address Details</h4>
                            <h6 className="header light">Enter your address details</h6>
                        </div>
                    </div>
                    <form onSubmit={this.nextClicked}>
                    
                    <div className="row">
                        <div className="input-field  col s12 m8 offset-m2">
                            <select className="browser-default">
                            <option value="" disabled defaultValue>Address Type</option>
                            <option value="Permanent">Permanent</option>
                            {/* <option value="Temporary">Temporary</option> */}
                            </select>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s12 m8 offset-m2">
                            <i className="material-icons prefix">email</i>
                            <input 
                                id="houseNo" 
                                type="text" 
                                className="validate" 
                                value={this.props.houseNumber}
                                onChange={(event) => this.props.houseNumberEntered(event.target.value)}
                                required />
                            <label htmlFor="houseNo">House Number</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s12 m8 offset-m2">
                            <i className="material-icons prefix">email</i>
                            <input 
                                id="street" 
                                type="text" 
                                className="validate" 
                                value={this.props.street}
                                onChange={(event) => this.props.streetEntered(event.target.value)} required/>
                            <label htmlFor="street">Street</label>
                        </div>
                    </div>
                    
                    <div className="row">
                        <div className="input-field col s12 m8 offset-m2">
                            <i className="material-icons prefix">email</i>
                            <input 
                                id="landmark" 
                                type="text" 
                                className="validate" 
                                value={this.props.landmark}
                                onChange={(event) => this.props.landmarkEntered(event.target.value)} />
                            <label htmlFor="landmark">Landmark</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s12 m8 offset-m2">
                            <i className="material-icons prefix">email</i>
                            <input 
                                id="dateOfBirth" 
                                type="text" 
                                className="validate" 
                                value={this.props.city}
                                onChange={(event) => this.props.cityEntered(event.target.value)}
                                required />
                            <label htmlFor="city">City</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s12 m8 offset-m2">
                            <i className="material-icons prefix">lock</i>
                            <input 
                                id="zipcode" 
                                type="number" 
                                className="validate" 
                                value={this.props.zipcode}
                                onChange={(event) => this.props.zipcodeEntered(event.target.value)}
                                required />
                            <label htmlFor="zipcode">Zipcode</label>
                        </div>
                    </div>

                    {
                        this.props.country != null 
                        ?   <div className="row">
                            <div className="input-field col s12 m8 offset-m2">
                                <Dropdown 
                                    data={this.props.country}
                                    type="COUNTRY" />
                            </div>
                            </div>
                        :   null
                    }

                    {
                        this.props.state != null && this.props.state.length > 0
                        ?   <div className="row">
                            <div className="input-field col s12 m8 offset-m2">
                                <Dropdown 
                                    data={this.props.state}
                                    type="STATE"
                                />
                            </div>
                            </div>
                        :   null
                    }
                    
                    <div className="row center">
                        <Spinner className={this.state.spinnerClass.join(" ")} />
                    </div>
                    
                    <div className="row center">
                        <div className="input-field col s12 m4 left">
                            <button 
                                className="btn waves-effect waves-light" 
                                name="action"
                                onClick={() => this.props.changePath("/edit/personal")}>Prev
                            <i className="material-icons left">chevron_left</i>
                            </button>
                        </div>
                        <div className="input-field col s12 m4 right">
                            <button className="btn waves-effect waves-light" type="submit" name="action">Next
                            <i className="material-icons right">chevron_right</i>
                            </button>
                        </div>
                    </div>
                    </form>
                </div>

                {/* <div className="section">
                    <div className="fixed-action-btn">
                        <a className="btn-floating btn-large red">
                            <i className="large material-icons">add</i>
                        </a>
                    </div>
                </div> */}
            </div>
        )

















        // return (
        //     <section class="main-auth-content">
        //         <div class="auth-content">
        //             <form>
        //                 <span class="auth-text">Enter your Address Details</span>
        //                 <InputTag
        //                     type="text"
        //                     placeholder="Type"
        //                     value={this.props.firstName}
        //                     onChange={(event) => this.props.typeEntered(event.target.value)}/>
        //                 <InputTag
        //                     type="text"
        //                     placeholder="House Number"
        //                     value={this.props.middleName}
        //                     onChange={(event) => this.props.houseNumberEntered(event.target.value)}/>
        //                 <InputTag
        //                     type="text"
        //                     placeholder="Street"
        //                     value={this.props.lastName}
        //                     onChange={(event) => this.props.streetEntered(event.target.value)}/>
        //                 <InputTag
        //                     type="text"
        //                     placeholder="Landmark"
        //                     value={this.props.lastName}
        //                     onChange={(event) => this.props.landmarkEntered(event.target.value)}/>
        //                 <InputTag
        //                     type="text"
        //                     placeholder="City"
        //                     value={this.props.dateOfBirth}
        //                     onChange={(event) => this.props.cityEntered(event.target.value)}/>
        //                 <InputTag
        //                     type="number"
        //                     placeholder="Zipcode"
        //                     value={this.props.lastName}
        //                     onChange={(event) => this.props.zipcodeEntered(event.target.value)}/>
        //                 {
        //                     this.props.country != null 
        //                     ?   <Dropdown 
        //                             data={this.props.country}
        //                             type="COUNTRY"
        //                         />
        //                     :   null
        //                 }

        //                 {
        //                     this.props.state != null && this.props.state.length > 0
        //                     ?   <Dropdown 
        //                             data={this.props.state}
        //                             type="STATE"
        //                         />
        //                     :   null
        //                 }
                        
                        
        //             </form>
        //             <div>
        //                 <Link to="/edit/personal"><button class="btn next-btn">PREV</button></Link>
        //                 <button class="btn next-btn" type="submit"  onClick={this.nextClicked}>NEXT</button>
        //             </div>

        //             <Spinner className={this.state.spinnerClass.join(" ")} />
        //         </div>
        //     </section>

        // )
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        typeEntered: AddressAction.typeEntered,
        houseNumberEntered: AddressAction.houseNumberEntered,
        streetEntered: AddressAction.streetEntered,
        landmarkEntered: AddressAction.landmarkEntered,
        cityEntered: AddressAction.cityEntered,
        zipcodeEntered: AddressAction.zipcodeEntered,
        addressDetailSubmitted: AddressAction.addressDetailSubmitted,

        fetchCountry: AddressAction.fetchCountry,
        changePath: changePath,
        fillAddressDetails: AddressAction.fillAddressDetails
    }, dispatch)
);

const mapStateToProps = state => ({
    type: state.addressDetail.type,
    houseNumber: state.addressDetail.houseNumber,
    street: state.addressDetail.street,
    landmark: state.addressDetail.landmark,
    city: state.addressDetail.city,
    state: state.addressDetail.state,
    zipcode: state.addressDetail.zipcode,
    country: state.addressDetail.country,
});

export default connect(mapStateToProps, mapDispatchToProps)(AddressDetail);