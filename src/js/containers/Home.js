import React from "react";
import { connect } from "react-redux";
import { Redirect, Switch, Link, Route} from "react-router-dom";
import { bindActionCreators } from "redux";
import { changePath } from "../actions/appStateAction";
import { logoutClicked } from "../actions/loginAction";
import ApplicationStatus from "../components/ApplicationStatus";
import PersonalDetail from "./PersonalDetail";
import AddressDetail from "./AddressDetail";
import AcademicDetail from "./AcademicDetail";
import DocumentDetail from "./DocumentDetail";
import Header from "../components/Header";
import Footer from "../components/Footer";
import Status from "../components/Status";
import { API } from "./../store/appState";
import { AddressAction } from "../actions/addressDetailAction";
import { fillPersonalDetails } from "../actions/personalDetailAction";
import { fillAcademicDetails } from "../actions/academicDetailAction";
import axios from 'axios';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.getView = this.getView.bind(this);
        this.state = {
            statusDate: null,
        }
        let statusAPI = API + "/user/secure/getRegistrationEndDate";
        axios.get(statusAPI).then(response => this.setState({
            statusDate: response.data
        }))
        this.logoutClicked = this.logoutClicked.bind(this);
        this.props.fillPersonalDetails();
        this.props.fillAddressDetails();
        this.props.fillAcademicDetails();
    }

    static getDerivedStateFromProps(props, state) {
        return state;
    }

    getView(path) {
        if (path === "/edit/personal") {
            return <PersonalDetail />
        } else if (path === "/edit/address") {
            return <AddressDetail />
        } else if (path === "/edit/academic") {
            return <AcademicDetail />
        } else if (path === "/edit/document") {
            return <DocumentDetail />
        } else if (path === "/edit/status") {
            return <Status date={this.state.statusDate}/>
        } else {
            return <Status date={this.state.statusDate}/>
        }
    }

    logoutClicked() {
        this.props.logoutClicked();
        window.location = API + "/logout";
    }
    render() {
        let { changePath } = this.props;
        if (!this.props.isLoggedIn) {
            this.props.changePath("/login");
            return null;
            // return <Redirect to="/login" />
        } else {
            return (
                <section>
                    <Header sideLinkLabel="Logout" sideLinkOnClick={this.logoutClicked}/>
                    <div id="slide-out" className="sidenav sidenav-fixed grey darken-3 home-side-nav">
                        <div className="valign-wrapper center">
                            <img className="epam-logo" src={require("../../images/EPAM_LOGO.png")} alt="epam-logo" />
                        </div>
                        <h5>
                            <a  className="light grey-text text-lighten-5" 
                                onClick={() => changePath("/edit/personal")}>Personal Details</a>
                            { this.props.isPersonalFilled
                                ? <i class="material-icons green-text">check</i>
                                : null
                            }
                        </h5>
                        <h5>
                            <a  className="light grey-text text-lighten-5"
                                onClick={() => changePath("/edit/address")} >Address Details</a>
                            { this.props.isAddressFilled
                                ? <i class="material-icons green-text">check</i>
                                : null
                            }
                        </h5>
                        <h5>
                            <a  className="light grey-text text-lighten-5"
                                onClick={() => changePath("/edit/academic")}>Academic Details</a>
                            { this.props.isAcademicFilled
                                ? <i class="material-icons green-text">check</i>
                                : null
                            }
                        </h5>
                        <h5>
                            <a  className="light grey-text text-lighten-5"
                                onClick={() => changePath("/edit/document")}>Document Upload</a>
                            { this.props.isDocumentUploaded
                                ? <i class="material-icons green-text">check</i>
                                : null
                            }
                        </h5>
                        <h5>
                            <a  className="light grey-text text-lighten-5" 
                                onClick={() => changePath("/edit/status")}>Status</a>
                        </h5>
                    </div>
                    <main className="home-main">
                        {   this.getView(this.props.viewPath)   }
                    </main>

                    <Footer />




                </section>
            );
        }
    }
}

const mapStateToProps = state => ({
    viewPath: state.appState.viewPath,

    isLoggedIn: state.appState.isLoggedIn,
    isPersonalFilled: state.appState.isPersonalFilled,
    isAddressFilled: state.appState.isAddressFilled,
    isAcademicFilled: state.appState.isAcademicFilled,
    isDocumentUploaded: state.appState.isDocumentUploaded,
})

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        logoutClicked: logoutClicked,
        changePath: changePath,
        fillAddressDetails: AddressAction.fillAddressDetails,
        fillPersonalDetails: fillPersonalDetails,
        fillAcademicDetails: fillAcademicDetails
    }, dispatch)
)
export default connect(mapStateToProps, mapDispatchToProps)(Home);