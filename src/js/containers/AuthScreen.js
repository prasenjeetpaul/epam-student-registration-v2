import React from "react";
import { Route, Switch } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { changePath } from "./../actions/appStateAction";
import Login from "./Login";
import Register from "./Register";
import AuthHeader from "../components/AuthHeader";
import AuthFooter from "../components/AuthFooter";
import AuthBanner from "../components/AuthBanner";

class AuthScreen extends React.Component {
    render() {
        return (
            <section>
                <AuthHeader linkName="Login Now" linkAction={() => this.props.changePath("/login")}/>
                <AuthBanner />
                <main>
                    {
                        this.props.viewPath === "/register"
                        ?   <Register />
                        :   <Register />
                    }
                    {/* <section className="main-auth-content">
                        <Switch>
                            <Route path="/login" component={Login} />
                            <Route path="/register" component={Register} />
                            <Route path="*" component={Login} />
                        </Switch>
                    </section> */}
                </main>
                <AuthFooter />
            </section>
        )
    }
}
const mapDispatchToProps = dispatch => (
    bindActionCreators({
        changePath: changePath
    }, dispatch)
);

const mapStateToProps = state => ({
    viewPath: state.appState.viewPath,
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthScreen);