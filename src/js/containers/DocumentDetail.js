import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { DocumentAction } from "../actions/documentDetailAction";
import Spinner from "../components/Spinner";
import { changePath } from "./../actions/appStateAction";
import M from "materialize-css";

class DocumentDetail extends React.Component {
    constructor(props) {
        super(props);
        this.uploadAadhar = this.uploadAadhar.bind(this);
        this.uploadResume = this.uploadResume.bind(this);
        this.nextClicked = this.nextClicked.bind(this);
        this.state = {
            spinnerClass: ["hide"],
        }

    }

    nextClicked(event) {
        event.preventDefault();
        this.setState({
            spinnerClass: [""]
        })
        this.props.documentUploaded(this.props.history)
    }

    uploadAadhar(event) {
        let file = event.target.files[0];
        if (file) {
            let data = new FormData();
            data.append('file', file);
            this.props.aadharUploaded(data);
        }
    }

    uploadResume(event) {
        let file = event.target.files[0];
        if (file) {
            let data = new FormData();
            data.append('file', file);
            this.props.resumeUploaded(data);
        }
    }


    render() {

        return (


            <div className="container home-detail">
                <div className="section card z-depth-3 auth-card">
        
                    <div className="row center">
                        <div className="col s12 m8 offset-m2">
                            <h4 className="header light">Document Details</h4>
                            <h6 className="header light">Upload your documents</h6>
                        </div>
                    </div>
                    <form onSubmit={this.nextClicked}>
                    
                    <div className="row">
                        <div className="input-field file-field col s12 m8 offset-m2">
                            <div className="btn">
                                <span>Your Govt. ID</span>
                                <input name="aadhar"
                                    type="file" 
                                    accept="image/*" 
                                    onChange={this.uploadAadhar}/>
                            </div>
                            <div className="file-path-wrapper">
                                <input className="file-path validate" type="text" />
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field file-field col s12 m8 offset-m2">
                            <div className="btn">
                                <span>Resume</span>
                                <input  name="resume" 
                                    type="file" 
                                    onChange={this.uploadResume} />
                            </div>
                            <div className="file-path-wrapper">
                                <input className="file-path validate" type="text" />
                            </div>
                        </div>
                    </div>


                    <div className="row center">
                        <Spinner className={this.state.spinnerClass.join(" ")} />
                    </div>
                    
                    <div className="row center">
                        <div className="input-field col s12 m4 left">
                            <button 
                                className="btn waves-effect waves-light" 
                                name="action"
                                onClick={() => this.props.changePath("/edit/academic")}>Prev
                            <i className="material-icons left">chevron_left</i>
                            </button>
                        </div>
                        <div className="input-field col s12 m4 right">
                            <button className="btn waves-effect waves-light" type="submit" name="action">Finish
                            <i className="material-icons right">chevron_right</i>
                            </button>
                        </div>
                    </div>
                    </form>



                </div>

            </div>


        )
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        aadharUploaded: DocumentAction.aadharUploaded,
        resumeUploaded: DocumentAction.resumeUploaded,
        documentUploaded: DocumentAction.documentUploaded,
        changePath: changePath,
    }, dispatch)
)

export default connect(null, mapDispatchToProps)(DocumentDetail);