const appState = {
    viewPath: "/",
    
    studentID: null,
    
    isLoggedIn: false,
    loginError: null,
    registerError: null,

    isPersonalFilled: false,
    isAddressFilled: false,
    isAcademicFilled: false,
    isDocumentUploaded: false,

    API: null
}

export default appState;

// export const API = "http://localhost:8089";

export const API =  document.getElementById("contextPath");
export const LoginAPI = API + "/welcome";