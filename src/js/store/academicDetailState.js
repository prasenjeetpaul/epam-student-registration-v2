const academicDetailState = {
    name: [],
    board: [],
    passOutYear: [],
    percentageOfMarks: [],
    isHighestEducation: []
}

export default academicDetailState;