const addressDetailState = {
    type: "",
    houseNumber: "",
    street: "",
    landmark: "",
    city: "",
    state: null,
    zipcode: "",
    country: null,

    selectedCountry: "",
    selectedState: "",
}

export default addressDetailState;