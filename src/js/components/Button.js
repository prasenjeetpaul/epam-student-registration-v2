import React from "react";

const Button = props => (
    <button
        className={"btn " + props.className}
        type = {
            typeof props.type != "undefined" 
            ? props.type
            : "" }
        onClick={props.onClick}>
        {props.label}
    </button>
)

export default Button;