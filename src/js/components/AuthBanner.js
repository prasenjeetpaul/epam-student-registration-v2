import React from "react";

const AuthBanner = () => (
    <div id="index-banner" className="parallax-container    valign-wrapper">
        <div className="section no-pad-bot">
            <div className="container">
                <div className=" ">
                        <div className="row center">
                            <img className="header center" src={require("../../images/EPAM_LOGO.png")} />
                            <h5 className="header col s12 light black-text">A modern responsive front-end framework based on Material Design</h5>
                        </div>
                </div>

            </div>
        </div>
        <div className="parallax"><img src={require("../../images/banner_1.jpg")} alt="Unsplashed background img 1" /></div>
    </div>
);

export default AuthBanner;