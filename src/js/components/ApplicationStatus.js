import React from "react";

const ApplicationStatus = props => (
    <section className="application-status">
        <span>Your application is under review! We'll get back to you shortly</span>
    </section>
);

export default ApplicationStatus;