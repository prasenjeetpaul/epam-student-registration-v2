import React from "react";

const AuthFooter = () => (
    <footer className="page-footer grey darken-3">
        <div className="container">
            <div className="row">
                <div className="col l6 s12">
                    <h5 className="white-text">EPAM System</h5>
                    <p className="grey-text text-lighten-4">
                        We help customers around the world become competitive – and stay competitive. We combine best-in-class software engineering with digital strategy and experience design, business consulting and technology innovation services.
                    </p>
                </div>
                {/* <div className="col l3 s12 right-align">
                    <h5 className="white-text">Settings</h5>
                    <ul>
                        <li><a className="white-text" href="#!">Link 1</a></li>
                        <li><a className="white-text" href="#!">Link 2</a></li>
                        <li><a className="white-text" href="#!">Link 3</a></li>
                        <li><a className="white-text" href="#!">Link 4</a></li>
                    </ul>
                </div> */}
                <div className="col l6 s12 right-align">
                    <h5 className="white-text">Connect</h5>
                    <ul>
                        <li><a className="white-text" href="https://welcome.epam.in/" target="_blank">EPAM India</a></li>
                        <li><a className="white-text" href="https://epam.com/" target="_blank">EPAM Global</a></li>
                        <li><a className="white-text" href="https://www.facebook.com/EPAM.India/" target="_blank">Facebook</a></li>
                        <li><a className="white-text" href="http://www.linkedin.com/company/4972?goback=.fcs_GLHD_EPAM_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2&trk=ncsrch_hits" target="_blank">LinkedIn</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div className="footer-copyright grey darken-4">
            <div className="container">
            &copy; <a className="brown-text text-lighten-3" >EPAM Systems</a>
            </div>
        </div>
    </footer>
)

export default AuthFooter;