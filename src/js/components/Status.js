import React from "react";

const Status = (props) => (
        <div className="container home-detail">
                <div className="section card z-depth-3 auth-card">
        
                    <div className="row center">
                        <div className="col s12 m8 offset-m2">
                            <h5 className="header light">
                            To know the status of your application, you can come back here by "{props.date}"" and check your status.
                            </h5>
                        </div>
                    </div>
                
                </div>
            </div>
);

export default Status;