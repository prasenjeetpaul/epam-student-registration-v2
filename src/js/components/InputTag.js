import React from "react";

const InputTag = props => (
    <input
        className={"auth-input " + props.className}
        type={props.type}
        onChange={props.onChange}
        placeholder={props.placeholder}
        value={props.value}/>
)

export default InputTag;