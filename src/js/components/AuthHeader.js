import React from "react";
import { LoginAPI } from "./../store/appState";

const AuthHeader = props => (
    <nav className="grey darken-3" role="navigation" >
    <div className="nav-wrapper container">
        <div id="logo-container" className="brand-logo">
            <h5 className="light logo-text">EPAM STUDENTS</h5>
        </div>
        <ul className="right hide-on-med-and-down">
            {/* <li><a onClick={props.linkAction}>{props.linkName}</a></li> */}
            <li><a href={ LoginAPI }>{props.linkName}</a></li>
        </ul>

        <ul id="nav-mobile" className="sidenav">
            <li><a href={ LoginAPI }>{props.linkName}</a></li>
            {/* <li><a onClick={props.linkAction}>{props.linkName}</a></li> */}
        </ul>
        <a href="#" data-target="nav-mobile" className="sidenav-trigger">
            <i className="material-icons">menu</i>
        </a>
    </div>
    </nav>
);

export default AuthHeader;