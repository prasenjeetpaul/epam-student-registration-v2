import React from "react";

const Footer = () => (
    <footer className="page-footer grey darken-3 home-footer">
        <div className="footer-copyright grey darken-4">
            <div className="container">
            &copy; <a className="brown-text text-lighten-3" href="http://materializecss.com">EPAM Systems</a>
            </div>
        </div>
    </footer>
);

export default Footer;