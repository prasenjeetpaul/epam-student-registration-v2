import React from "react";

const DropdownItem = props => (
    <option key={props.name}>
        {props.name}
    </option>
);

export default DropdownItem;