import React from "react";

const ErrorMessage = props => (
    <div className="row right-align">
        <div className="col s12 m8 offset-m2">
            <span className="red-text">{props.message}</span>
        </div>
    </div>
);

export default ErrorMessage;