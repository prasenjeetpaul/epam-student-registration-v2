import { PERSONAL_DETAIL_ACTION, APP_STATE_ACTION } from './actionType';
import { API } from "./../store/appState";
import { changePath } from "./appStateAction";
import axios from 'axios';

require ("babel-polyfill");


export const firstNameEntered = firstName => ({
    type: PERSONAL_DETAIL_ACTION.FIRST_NAME_ENTERED,
    payload: firstName
})

export const middleNameEntered = middleName => ({
    type: PERSONAL_DETAIL_ACTION.MIDDLE_NAME_ENTERED,
    payload: middleName
})

export const lastNameEntered = lastName => ({
    type: PERSONAL_DETAIL_ACTION.LAST_NAME_ENTERED,
    payload: lastName
})

export const dateOfBirthEntered = dateOfBirth => ({
    type: PERSONAL_DETAIL_ACTION.DATE_OF_BIRTH_ENTERED,
    payload: dateOfBirth
})

export const mobileNumberEntered = mobileNumber => ({
    type: PERSONAL_DETAIL_ACTION.MOBILE_NUMBER_ENTERED,
    payload: mobileNumber
})


export const fillPersonalDetails = () => (dispatch, getState) => {
    let personalDataFetchAPI = API + "/user/secure/personalInfo?id=" + getState().appState.studentID;
    axios.get(personalDataFetchAPI)  .then(response => response.data)
        .then(data => {
            if (data==null || data.length==0) {
                return;
            }
            delete data.id;
            dispatch({
                type: PERSONAL_DETAIL_ACTION.PERSONAL_DATA_LOAD,
                payload: data,
            });
            dispatch({
                type: APP_STATE_ACTION.PEROSNAL_DATA_FILLED,
                payload: true
            });;
        });
}


export function fakeAPI() {
    return new Promise(resolve => {
        setTimeout(() => {
          resolve('resolved');
        }, 2000);
    });
}

export const personalDetailSubmitted = (history) => (dispatch, getState) => {
    let personalDetailAPI = API + "/user/secure/personalInfo";
    axios.post(personalDetailAPI, {
            id: getState().appState.studentID,
            ...getState().personalDetail
        })  .then(response => response.data)
            .then(data => {
                console.log(data);
                if (data) {
                    dispatch({
                        type: APP_STATE_ACTION.PEROSNAL_DATA_FILLED,
                        payload: true
                    });
                    dispatch(changePath("/edit/address"));
                }
            });
        // history.push("/edit/address");
    ;
    // axios.post(personalDetailAPI, {
    //     method: "POST",
    //     headers: {
    //         "Content-Type": "application/json"
    //     },
    //     body: JSON.stringify({
    //         id: getState().appState.studentID,
    //         ...getState().personalDetail
    //     })
    // }).then(response => response.text().then(data => {
    //     dispatch({
    //         type: APP_STATE_ACTION.PEROSNAL_DATA_FILLED,
    //         payload: true
    //     });
    //     history.push("/edit/address");
    // }));


    // fakeAPI().then(data => {
    //     console.log("Submitted Data");
    //     console.log(getState().addressDetail);
    //     if (data != null) {
    //         dispatch({
    //             type: APP_STATE_ACTION.ADDRESS_DATA_FILLED,
    //             payload: true
    //         });
    //         dispatch(changePath("/edit/address"));
    //     }
    // });
}