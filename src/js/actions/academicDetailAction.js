import { ACADEMIC_DETAIL_ACTION, APP_STATE_ACTION} from "./actionType";
import { API } from "./../store/appState";
import axios from 'axios';
import { changePath } from "./appStateAction";

export const nameEntered = (name, id) => (dispatch, getState) => {
    id = id-1;
    let nameArray = getState().academicDetail.name;
    for (var i=0; i<=id; i++) {
        if (i==id) {
            nameArray[i] = name
        }
    }
    dispatch({
        type: ACADEMIC_DETAIL_ACTION.NAME_ENTERED,
        payload: nameArray,
    })
}

export const boardEntered = (board, id) => (dispatch, getState) => {
    id = id-1;
    let boardArray = getState().academicDetail.board;
    for (var i=0; i<=id; i++) {
        if (i==id) {
            boardArray[i] = board
        }
    };
    dispatch({
        type: ACADEMIC_DETAIL_ACTION.BOARD_ENTERED,
        payload: boardArray
    })
}

export const passOutYearEntered = (passOutYear, id) => (dispatch, getState) => {
    id = id-1;
    let passoutArray = getState().academicDetail.passOutYear;
    for (var i=0; i<=id; i++) {
        if (i==id) {
            passoutArray[i] = passOutYear
        }
    };
    dispatch({
        type: ACADEMIC_DETAIL_ACTION.PASS_OUT_YEAR_ENTERED,
        payload: passoutArray
    })
}

export const percentageOfMarksEntered = (percentageOfMarks, id) => (dispatch, getState) => {
    id = id-1;
    let percentageArray = getState().academicDetail.percentageOfMarks;
    for (var i=0; i<=id; i++) {
        if (i==id) {
            percentageArray[i] = percentageOfMarks
        }
    };
    dispatch({
        type: ACADEMIC_DETAIL_ACTION.PERCENTAGE_OF_MARKS_ENTERED,
        payload: percentageArray
    })
}

export const deleteRecord =  id => (dispatch, getState) =>{
    let nameArray = getState().academicDetail.name;
    let boardArray = getState().academicDetail.board;
    let passoutArray = getState().academicDetail.passOutYear;
    let percentageArray = getState().academicDetail.percentageOfMarks;
    let isHighestArray = getState().academicDetail.isHighestEducation;

    nameArray.splice( id, 1 );
    boardArray.splice( id, 1 );
    passoutArray.splice( id, 1 );
    percentageArray.splice( id, 1);
    isHighestArray.splice( id, 1 );

    dispatch({
        type: ACADEMIC_DETAIL_ACTION.NAME_ENTERED,
        payload: nameArray
    })

    dispatch({
        type: ACADEMIC_DETAIL_ACTION.BOARD_ENTERED,
        payload: boardArray
    })

    dispatch({
        type: ACADEMIC_DETAIL_ACTION.PASS_OUT_YEAR_ENTERED,
        payload: passoutArray
    })

    dispatch({
        type: ACADEMIC_DETAIL_ACTION.PERCENTAGE_OF_MARKS_ENTERED,
        payload: percentageArray
    })

    dispatch({
        type: ACADEMIC_DETAIL_ACTION.IS_HIGHEST_EDUCATION_ENTERED,
        payload: isHighestArray
    })

}

export const isHighestEducationEntered = (isHighestEducation, id) => (dispatch, getState) => {
    id = id-1;
    let isHighestArray = getState().academicDetail.isHighestEducation;
    for (var i=0; i<=id; i++) {
        if (i==id) {
            isHighestArray[i] = isHighestEducation
        }
    };
    
    dispatch({
        type: ACADEMIC_DETAIL_ACTION.IS_HIGHEST_EDUCATION_ENTERED,
        payload: isHighestArray
    })
}


export const academicDetailSubmitted = (history) => (dispatch, getState) => {
    let academicDetailAPI = API + "/user/secure/academicInfo";
    let counter = 0;
    
    for (let i=0; i<getState().academicDetail.board.length; i++) {
        let data = {
            userId: getState().appState.studentID,
            name: getState().academicDetail.name[i],
            board: getState().academicDetail.board[i],
            passOutYear: getState().academicDetail.passOutYear[i],
            percentageOfMarks: getState().academicDetail.percentageOfMarks[i],
            isHighestEducation: getState().academicDetail.isHighestEducation[i],
        }
        console.log(data)
        axios.post(academicDetailAPI, data).then(response => response.data)
            .then(data => {
                if (data) {
                    counter++;
                    if (counter === getState().academicDetail.board.length) {
                        dispatch({
                            type: APP_STATE_ACTION.ACADEMIC_DATA_FILLED,
                            payload: true
                        });
                        dispatch(changePath("/edit/document"));
                    }
                }
            })
    }
    
    // fetch(personalDetailAPI, {
    //     method: "POST",
    //     headers: {
    //         "Content-Type": "application/json"
    //     },
    //     body: JSON.stringify({
    //         id: getState().appState.studentID,
    //         ...getState().academicDetail
    //     })
    // }).then(response => response.text().then(data => {
    //     dispatch({
    //         type: APP_STATE_ACTION.ACADEMIC_DATA_FILLED,
    //         payload: true
    //     });
    //     history.push("/edit/document");
    // }))

}

export const fillAcademicDetails = () => (dispatch, getState) => {
    let personalDataFetchAPI = API + "/user/secure/academicInfo?id=" + getState().appState.studentID;
    axios.get(personalDataFetchAPI)  .then(response => response.data)
        .then(data => {
            if (data==null || data.length==0) {
                return;
            }
            delete data.id;
            delete data.userId
            dispatch({
                type: APP_STATE_ACTION.ACADEMIC_DATA_FILLED,
                payload: true
            });;
        });
}


export const Academic_Action = {
    nameEntered: nameEntered,
    boardEntered: boardEntered,
    passOutYearEntered: passOutYearEntered,
    percentageOfMarksEntered: percentageOfMarksEntered,
    isHighestEducationEntered: isHighestEducationEntered,
    academicDetailSubmitted: academicDetailSubmitted,
    fillAcademicDetails: fillAcademicDetails,
    deleteRecord: deleteRecord,
}