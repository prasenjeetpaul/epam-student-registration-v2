import { REGISTER_ACTION, APP_STATE_ACTION } from "./actionType";
import  { API } from "./../store/appState";
import axios from 'axios';
import M from "materialize-css";

require('es6-promise').polyfill();
require('isomorphic-fetch');

export const registerEmailEntered = emailId => ({
    type: REGISTER_ACTION.EMAIL_ENTERED,
    payload: emailId
})

export const registerPassword1Entered = password => ({
    type: REGISTER_ACTION.PASSWORD1_ENTERED,
    payload: password
})

export const registerPassword2Entered = password => ({
    type: REGISTER_ACTION.PASSWORD2_ENTERED,
    payload: password
})

export const saveUserID = id => ({
    type: APP_STATE_ACTION.REGISTER_SUCCESS,
    payload: id
})

export const saveAPI = API => ({
    type: APP_STATE_ACTION.SAVE_API_LINK,
    payload: API
})

export const registerClicked = () => (dispatch, getState) => {
    if (getState().register.password2 != getState().register.password1) {
        dispatch({
            type: APP_STATE_ACTION.REGISTER_FAILED,
            payload: "Password didn't matched"
        })
    } else {
        let verifyEmail = API + "/verifyEmail";
        axios.post(verifyEmail, {
                emailId: getState().register.emailId
        })  .then(response => response.data)
            .then(data => {
                {
                    if(data) {
                        let registerUser = API + "/registration";
                        axios.post(registerUser, {
                            emailId: getState().register.emailId,
                            password: getState().register.password1
                        })  .then(response => response.data)
                            .then(data => {
                                if (data == "false") {
                                    dispatch({
                                        type: APP_STATE_ACTION.REGISTER_FAILED,
                                        payload: "Invalid inputs"
                                    })
                                } else {
                                    console.log(data);
                                    M.toast({html: 'Registration Successful', classes: 'rounded'});
                                    dispatch({
                                        type: APP_STATE_ACTION.REGISTER_SUCCESS,
                                        payload: data
                                    })
                                }
                            })
                    } else {
                        dispatch({
                            type: APP_STATE_ACTION.REGISTER_FAILED,
                            payload: "Email ID already exists"
                        })
                    }
                }
            })
            .catch(() => console.log("Can’t access the server. Blocked by browser?"))
    }
}