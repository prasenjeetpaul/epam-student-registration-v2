import { APP_STATE_ACTION } from "./actionType";
require('es6-promise').polyfill();
require('isomorphic-fetch');

export const changePath = path => ({
    type: APP_STATE_ACTION.PATH_CHANGED,
    payload: path
})

